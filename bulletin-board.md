# CSE 109 - Bulletin Board

## :computer: Homework

- [x] 05/07 - [Homework 9](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-9) - File Server Client
- [x] 04/23 - [Homework 8](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-8) - Socket Lab
- [x] 04/12 - [Homework 7](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-7) - Serde - [Solutions](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-7/-/tree/solutions)
- [x] 03/30 - [Homework 6](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-6) - Hash Set - [Solutions](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-6/-/tree/solutions)
- [x] 03/14 - [Homework 5](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-5) - Exploring GCC (Updated Due Date)
- [x] 03/03 - [Homework 4](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-4) - Linked List - [Solutions](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-4/-/tree/solutions)
- [x] 02/24 - [Homework 3](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-3) - Pointers - [Solutions](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-3/-/tree/solutions)
- [x] 02/17 - [Homework 2](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-2) - my_which (Updated Due Date) - [Solutions](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/homework-2/-/tree/solutions)
- [x] 02/08 - [Homework 1](https://gitlab.com/lehigh-cse109/fall-2021/assignments/homework-1) - Learning Git
- [x] 02/03 - [Homework 0](https://gitlab.com/lehigh-cse109/fall-2021/course-info/-/blob/master/Homework-0.md) - Sign up for Gitlab

## :checkered_flag: Quizzes and Exams

- [ ] 05/15 - [Final Exam](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/final-exam)
- [x] 04/29 - [Quiz 10](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-10)
- [x] 04/22 - [Quiz 9](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-9)
- [x] 04/15 - [Quiz 8](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-8)
- [x] 04/08 - [Quiz 7](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-7)
- [x] 03/31 - [Quiz 6](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-6)
- [x] 03/17 - [Midterm Exam](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/midterm-exam)
- [x] 03/09 - [Quiz 5](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-5)
- [x] 03/02 - [Quiz 4](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-4)
- [x] 02/23 - [Quiz 3](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-3) - [Solutions](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-3/-/blob/solutions)
- [x] 02/16 - [Quiz 2](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-2) - [Solutions](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-2/-/blob/solutions)
- [x] 02/09 - [Quiz 1](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-1) - [Solutions](https://gitlab.com/lehigh-cse-109/spring-2021/assignments/quiz-1/-/tree/solutions)

## :books: Readings

| Week                      | Readings           | 
| ------------------------- | ------------------ |
| Week 12 | <ul><li>[fork() man page](https://www.man7.org/linux/man-pages/man2/fork.2.html)<li></ul>
| Week 11 | <ul><li>[Unix Sockets Overview](https://www.tutorialspoint.com/unix_sockets/socket_quick_guide.htm)</li><li>[GNU Unix Socket Reference](https://www.gnu.org/software/libc/manual/html_node/Sockets.html)</li><ul>
| Week 10 | <ul><li>[File I/O in C - stdio.h Reference](https://www.cplusplus.com/reference/cstdio/)</li><li>[File I/O in C++ - fstream Reference](https://www.cplusplus.com/reference/fstream/fstream)</li><li>[Stream I/O in C++ - iostream Reference](https://www.cplusplus.com/reference/iostream/)</li><li>[C++ - File I/O Tutorial](https://www.cplusplus.com/doc/tutorial/files/)</li><li>[C++ - Stream I/O Tutorial](https://www.cplusplus.com/doc/tutorial/basic_io/)</li></ul>
| Week 9 | <ul><li>[C++ Bitwise Operators](https://www.learncpp.com/cpp-tutorial/bitwise-operators/)</li><li>[Bitwise Operator Tricks](https://codeforwin.org/2018/05/10-cool-bitwise-operator-hacks-and-tricks.html)</li><li>[C++ Vector Reference](https://www.cplusplus.com/reference/vector/vector/)</li><li>[Creation and Use of C++ Vector](https://www.bitdegree.org/learn/c-plus-plus-vector)</li><li>[Serialization and Deserialization](https://web.archive.org/web/20150405013606/http://isocpp.org/wiki/faq/serialization)</li></ul>
| Week 8 | <ul><li>[C++ Classes and Objects](https://www.tutorialspoint.com/cplusplus/cpp_classes_objects.htm)</li><li>[C++ Namespaces](https://www.tutorialspoint.com/cplusplus/cpp_namespaces.htm)</li><li>[C++ Templates](https://www.tutorialspoint.com/cplusplus/cpp_templates.htm)</li><li>[C++ Dynamic Memory](https://www.tutorialspoint.com/cplusplus/cpp_dynamic_memory.htm)</li><li>[Hash Tables](http://craftinginterpreters.com/hash-tables.html)</li></ul>
| Week 6 | <ul><li>[Learning GDB](https://www.tutorialspoint.com/gnu_debugger/index.htm)</li><li>[GDB Quick Reference](http://users.ece.utexas.edu/~adnan/gdb-refcard.pdf)</li></ul>
| Week 5 | <ul><li>[The C Preprocessor](https://www.math.utah.edu/docs/info/cpp_1.html)</li><li>[From Source to Binary: The Inner Workings of GCC](https://web.archive.org/web/20160410185222/https://www.redhat.com/magazine/002dec04/features/gcc/)</li><li>[The C Parser](https://github.com/gcc-mirror/gcc/blob/master/gcc/c/c-parser.c)</li><li>[ASCII Table](https://www.ascii-code.com)</li></ul>
| Week 4 | <ul><li>[Think C](https://github.com/tscheffl/ThinkC/blob/master/PDF/Think-C.pdf) - chapter 9.</li><li>[More Datatypes](http://crasseux.com/books/ctutorial/More-data-types.html#More%20data%20types)</li><li>[Data Structures](http://crasseux.com/books/ctutorial/Data-structures.html#Data%20structures)</li></ul>
| Week 3 | <ul><li>[Think C](https://github.com/tscheffl/ThinkC/blob/master/PDF/Think-C.pdf) - chapters 6-8.</li><li>[Pointers](http://crasseux.com/books/ctutorial/Pointers.html#Pointers)</li><li>[Arrays](http://crasseux.com/books/ctutorial/Arrays.html#Arrays)</li></ul>
| Week 2 | <ul><li>[C/C++ Extension for VSC](https://marketplace.visualstudio.com/items?itemName=ms-vscode.cpptools) (Good resource for getting your VSC environment configured)</li><li>[Think C](https://github.com/tscheffl/ThinkC/blob/master/PDF/Think-C.pdf) - chapters 1-5. This should be mostly review as it is analogous to Java</li><li>[Putting a Program Together](http://crasseux.com/books/ctutorial/Putting-a-program-together.html#Putting%20a%20program%20together) - this is new to the C language compared to Java</li></ul>
| Week 1 | [The Missing Semester](https://missing.csail.mit.edu) - Chapters 1, 2, 5, 6 |

## :vhs: Lectures - [Playlist](https://youtube.com/playlist?list=PL4A2v89SXU3TS1fcYFmXi-tch8p0LucE7)

| Item                      | Date              | Content          | Links          |
| ------------------------- | ------------------ | ------------------ | --------------- |
| **Week 14**|
| Recitation 14 | 5/7 | Recitation 14 | [Video](https://drive.google.com/file/d/1hbPM9YW3eZ3eyyRHmueaDaXJeR94OL4a/view?usp=sharing)
| Lecture 23 | 5/5 | Review Part II | [Video](https://youtu.be/sLDJAng9524)
| Lecture 22 | 5/3 | Review Part I | [Video](https://youtu.be/NdDw0o672WM)
| **Week 13**|
| Recitation 13 | 4/30 | Recitation 13 | [Video](https://drive.google.com/file/d/1tbXABO_Id3MfTB0M3Y43oq-wxeujRVxg/view?usp=sharing)
| Lecture 23 | 4/28 | Modern C++, Cont. | [Video](https://youtu.be/4Alu168qI6w)
| Lecture 22 | 4/26 | Modern C++ | [Video](https://youtu.be/oFqVZexEXPY)
| **Week 12**|
| Recitation 12 | 4/23 | Recitation 12 | [Video](https://drive.google.com/file/d/1om4beU7q0MYGoY1rEvGQ-XylD_G9aNi9/view?usp=sharing)
| Lecture 21 | 4/21 | Spawning Processes Using fork() | [Video](https://youtu.be/blF0ZZuzByY)
| Lecture 20 | 4/19 | Multitasking on a CPU | [Video](https://youtu.be/We3oYauyIyc)
| **Week 11**|
| Recitation 11 | 4/16 | Recitation 11 | [Video](https://drive.google.com/file/d/10F_GuON-SKAc9iTRxGTIp7sns069OWRz/view?usp=sharing)
| Lecture 20 | 4/14 | Communication over Unix Sockets, Cont. | [Video](https://youtu.be/-pfd_hI4faM)
| Lecture 19 | 4/12 | Communication over Unix Sockets | [Video](https://youtu.be/Qwv1yxjqnow)
| **Week 10**|
| Recitation 10 | 4/9 | Recitaiton 10 | [Video](https://drive.google.com/file/d/1IgvexKPrDtgPQpGsN72LTf4MrDbQVaFV/view?usp=sharing)
| Lecture 18 | 4/7 | Input/Output in C and C++, Cont. | [Video](https://youtu.be/SQlIHSAuRIc)
| Lecture 17 | 4/5 | Input/Output in C and C++ | [Video](https://youtu.be/Kz1Vzf1eoN4)
| **Week 9** |
| Reciation 9 | 4/2 | Recitation 9 | [Video](https://drive.google.com/file/d/1o-R29gHGgUv1ncEgJeZZwN18BJJf9iJq/view?usp=sharing)
| Lecture 16 | 3/31 | Serialization/Deserialization | [Video](https://youtu.be/k31cMIxn3SY)
| Lecture 15 | 3/29 | Bitwise Operators | [Video](https://youtu.be/CgUMgsdApM0)
| **Week 8**|
| Recitation 8 | 3/26 | Recitation 8 | [Video](https://drive.google.com/file/d/1jwkMXD0fKTV_sjsQAjm0hVapymGIqyjc/view?usp=sharing)
| Lecture 14 | 3/24 | Hash Tables | [Video](https://youtu.be/aKhcSiOb6kI)
| Lecture 13 | 3/22 | Intro to C++ | [Video](https://youtu.be/tYKayLg1tyA) - [Code](https://gitlab.com/lehigh-cse-109/spring-2021/course-info/-/tree/master/lecture-code/lecture13)
| **Week 7**|
| Recitation 7 | 3/19 | Recitation 7 | [Video](https://drive.google.com/file/d/1qjNm5ADRBxEvZrtbyYpJ7c0Or9eTqz0A/view?usp=sharing)
| **Week 6**|
| Recitation 6 | 3/12 | Recitation 6 | [Video](https://drive.google.com/file/d/1ZnrqfRjaL9umAIXEzTGjAWQv7zAYbP-c/view?usp=sharing)
| Lecture 12 | 3/10 | The GNU Debugger (gdb), Cont. | [Video](https://youtu.be/k5iTj0QQNSk)
| Lecture 11 | 3/8 | The GNU Debugger (gdb) | [Video](https://youtu.be/aInDJiXNcoA)
| **Week 5**|
| Recitation 5 | 3/5 | Recitation 5 | [Video](https://drive.google.com/file/d/10DMfbwolg9BWQuy8wqd2uzcng6nkq5Zd/view?usp=sharing)
| Lecture 10 | 3/3 | Compiling C Programs, Cont. | [Video](https://youtu.be/e9ghhbopWbg)
| Lecture 9 | 3/1 | Compiling C Programs | [Video](https://youtu.be/Hukb_0YFbhE)
|**Week 4**|
| Recitation 4 | 2/26 | Recitation 4 | [Video](https://drive.google.com/file/d/14q0x2HdXLNRQuPNYIPwZj0DC6NiOUp3B/view?usp=sharing)
|Lecture 8 | 2/24 | Memory Allocation in C | [Video](https://youtu.be/tNZw1RknRzQ)
|Lecture 7 | 2/22 | Enums, Unions, Structs | [Video](https://youtu.be/8ebwJ7pWgyU)
|**Week 3**|
|Recitation 3 | 2/19 | Recitation 3 | [Video](https://drive.google.com/file/d/14x7sOBb86ZbBgj0u0o17jp4TcEmHE9Be/view?usp=sharing)
|Lecture 6 | 2/17 | Pointers Cont. | [Video](https://youtu.be/bXt4uFQNhog)
|Lecture 5 | 2/15 | Pointers | [Video](https://youtu.be/AQ6WUlIT6i8)
|**Week 2**|
|Recitation 2 | 2/12 | Recitation 2 | [Video](https://drive.google.com/file/d/1IAuWqED9YbtYEbfB4bckji91xBIJZVKJ/view?usp=sharinghttps://drive.google.com/file/d/1IAuWqED9YbtYEbfB4bckji91xBIJZVKJ/view?usp=sharing) - [Code](https://gitlab.com/lehigh-cse-109/spring-2021/course-info/-/blob/master/lecture-code/recitation2/main.c)
|Lecture 4 | 2/10 | Anatomy of a C Program | [Video](https://youtu.be/Xh1LqbqKlo0) - [Code](https://gitlab.com/lehigh-cse-109/spring-2021/course-info/-/blob/master/project-template.zip)
|Lecture 3 | 2/8 | C and Unix | [Video](https://youtu.be/Bd21jOGT_GU)
|**Week 1**|
|Recitation 1 | 2/5 | Recitation 1 | [Video](https://drive.google.com/file/d/1eveEE1oJ9hIcRFamBZuaotOSMzCbhwGz/view?usp=sharing)
|Lecture 2 | 2/3 | Introduction to Git and C | [Video](https://youtu.be/Y0pARcUxQmo)
|Lecture 1 | 2/1 | Course Introduction | [Video](https://youtu.be/0eGRozEYNxA)

